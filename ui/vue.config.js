module.exports = {
  transpileDependencies: ["vuetify"],
  chainWebpack: config => {
    const oneOfsMap = config.module.rule("scss").oneOfs.store;
    oneOfsMap.forEach(item => {
      item
        .use("sass-resources-loader")
        .loader("sass-resources-loader")
        .options({
          resources: ["./node_modules/vuetify/src/styles/styles.sass"]
        })
        .end();
    });
  }
};
