export default function toTelephone(value) {
  let result = "";
  for (let i = 0; i < value.length; i++) {
    if (i != 0 && i % 2 === 0) result += " ";
    result += value[i];
  }
  return result;
}
