const Notify = {
  install(Vue) {
    let notify = () => null;

    Vue.prototype.$notify = (x) => notify(x);
    Vue.prototype.$setNotify = (listener) => {
      notify = listener;
    };
  },
};

export default Notify;
