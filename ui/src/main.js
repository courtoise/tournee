import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import { createProvider } from "./vue-apollo";
import { Icon } from "leaflet";
import "leaflet/dist/leaflet.css";
import routes from "./routes";
import Notify from "./notify";

delete Icon.Default.prototype._getIconUrl;
Icon.Default.mergeOptions({
  iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
  iconUrl: require("leaflet/dist/images/marker-icon.png"),
  shadowUrl: require("leaflet/dist/images/marker-shadow.png"),
});

Vue.use(VueRouter);
Vue.use(Notify);
Vue.config.productionTip = false;

const router = new VueRouter({ routes });

new Vue({
  vuetify,
  router,
  apolloProvider: createProvider(),
  render: (h) => h(App),
}).$mount("#app");
