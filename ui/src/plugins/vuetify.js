import "material-design-icons-iconfont/dist/material-design-icons.css";
import Vue from "vue";
import Vuetify from "vuetify/lib";
import fr from "vuetify/es5/locale/fr";

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: "md"
  },
  lang: {
    locales: { fr },
    current: "fr"
  },
  theme: {
    options: {
      customProperties: true
    }
  }
  // theme: {
  //   themes: {
  //     light: {
  //       primary: "#8bc34a",
  //       secondary: "#00bcd4",
  //       accent: "#ffc107",
  //       error: "#f44336",
  //       warning: "#ff9800",
  //       info: "#3f51b5",
  //       success: "#4caf50"
  //     }
  //   }
  // }
});
