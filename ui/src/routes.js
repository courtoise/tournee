import { formatISO, format } from "date-fns";

import Vehicules from "./components/vehicules/Vehicules";
import NouveauVehiculeFab from "./components/vehicules/NouveauVehiculeFab";
import Tournees from "./components/tournees/Tournees";
import Itineraires from "./components/tournees/Itineraires";
import ImprimerFab from "./components/tournees/ImprimerFab";
import Collectes from "./components/collectes/Collectes";
import CollecteFab from "./components/collectes/CollecteFab";
import CollecteEnregistrement from "./components/collectes/Enregistrement";
import Zones from "./components/zones/Zones";
import NouvelleZoneFab from "./components/zones/NouvelleZoneFab";
import Tournee from "./components/tournee/Tournee";
import Rapport from "./components/rapport/Rapport";

export default [
  { path: "/", redirect: "/tournees" },
  {
    path: "/vehicules",
    components: { default: Vehicules, fab: NouveauVehiculeFab },
  },
  {
    path: "/vehicules/nouveau",
    components: { default: Vehicules, fab: NouveauVehiculeFab },
  },
  { path: "/tournee/:id", component: Tournee },
  {
    path: "/tournees",
    redirect: () =>
      `/tournees/${formatISO(new Date(), { representation: "date" })}`,
  },
  { path: "/tournees/:date", component: Tournees },
  {
    path: "/tournee/:id/itineraires",
    components: { default: Itineraires, fab: ImprimerFab },
  },
  { path: "/collectes", components: { default: Collectes, fab: CollecteFab } },
  { path: "/collectes/enregistrement", component: CollecteEnregistrement },
  { path: "/collectes/:id/modification", component: CollecteEnregistrement },
  { path: "/zones", components: { default: Zones, fab: NouvelleZoneFab } },
  {
    path: "/zones/nouvelle",
    components: { default: Zones, fab: NouvelleZoneFab },
  },
  {
    path: "/rapport",
    redirect: () => `/rapport/${format(new Date(), "yyyy")}`,
  },
  {
    path: "/rapport/:annee",
    components: { default: Rapport, fab: ImprimerFab },
  },
];
