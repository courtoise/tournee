package api

import (
	"context"
	"errors"

	"github.com/graph-gophers/graphql-go"
)

// VehiculeResolver resolver
type VehiculeResolver struct {
	Vehicule
}

//Caracteristiques VehiculeResolver
func (v VehiculeResolver) Caracteristiques() ([]string, error) {
	var caracteristiques []Caracteristique
	if err := DB.Model(&v.Vehicule).Association("Caracteristiques").Find(&caracteristiques).Error; err != nil {
		return nil, err
	}
	var labels []string
	for _, caracteristique := range caracteristiques {
		labels = append(labels, caracteristique.Label)
	}
	return labels, nil
}

func toVehiculeResolvers(vehicules []Vehicule) []VehiculeResolver {
	var vehiculeResolvers []VehiculeResolver
	for _, vehicule := range vehicules {
		vehiculeResolvers = append(vehiculeResolvers, VehiculeResolver{
			vehicule,
		})
	}
	return vehiculeResolvers
}

// Vehicules resolver
func (r *Resolver) Vehicules(ctx context.Context) ([]VehiculeResolver, error) {
	var vehicules []Vehicule

	if err := DB.Order("modele").Find(&vehicules).Error; err != nil {
		return nil, err
	}

	return toVehiculeResolvers(vehicules), nil
}

// AjouterVehicule mutation
func (r *Resolver) AjouterVehicule(ctx context.Context, args struct{ Modele string }) (*VehiculeResolver, error) {
	vehicule := Vehicule{
		Modele:   args.Modele,
		Quantite: 1,
	}

	if err := DB.Create(&vehicule).Error; err != nil {
		return nil, err
	}

	return &VehiculeResolver{
		vehicule,
	}, nil
}

// AugmenterVehiculeQuantite ajoute 1 à la quantité
func (r *Resolver) AugmenterVehiculeQuantite(ctx context.Context, args struct{ ID graphql.ID }) (*VehiculeResolver, error) {
	var vehicule Vehicule

	if err := DB.Take(&vehicule, "id = ?", args.ID).Error; err != nil {
		return nil, err
	}

	vehicule.Quantite++

	if err := DB.Save(&vehicule).Error; err != nil {
		return nil, err
	}

	return &VehiculeResolver{
		vehicule,
	}, nil
}

// ReduireVehiculeQuantite réduit 1 à la quantité
func (r *Resolver) ReduireVehiculeQuantite(ctx context.Context, args struct{ ID graphql.ID }) (*VehiculeResolver, error) {
	var vehicule Vehicule

	if err := DB.Take(&vehicule, "id = ?", args.ID).Error; err != nil {
		return nil, err
	}

	if vehicule.Quantite == 1 {
		return nil, errors.New("La quantité de véhicule minimum est atteinte")
	}

	vehicule.Quantite--

	if err := DB.Save(&vehicule).Error; err != nil {
		return nil, err
	}

	return &VehiculeResolver{
		vehicule,
	}, nil
}

// SupprimerVehicule supprime le vehicule
func (r *Resolver) SupprimerVehicule(ctx context.Context, args struct{ ID graphql.ID }) (*bool, error) {
	if err := DB.Delete(&Vehicule{}, "id = ?", args.ID).Error; err != nil {
		return nil, err
	}

	success := true
	return &success, nil
}
