schema {
  query: Query
  mutation: Mutation
}

# scalar Time
scalar Date

type Query {
  healthy: Boolean!
  vehicules: [Vehicule!]!
  caracteristiques: [String!]!
  zones: [Zone!]!
  tournees(zoneId: ID, annee: Int!, mois: Int!, existante: Boolean): [Tournee!]!
  tournee(id: ID!): Tournee!
  adresse(recherche: String!): [Adresse!]!
  commune(recherche: String!): [String!]!
  collectes(tourneeId: ID): [Collecte!]!
  collecte(id: ID!): Collecte
  itineraires(tourneeId: ID!): [Itineraire!]!
  rapport(annee: Int!): [LigneRapport!]!
}

type Mutation {
  ajouterCaracteristique(label: String!): String
  modifierCaracteristique(ancienLabel: String!, nouveauLabel: String!): String
  supprimerCaracteristique(label: String!): Boolean
  ajouterVehiculeCaracteristique(id: ID!, caracteristique: String!): Vehicule
  enleverVehiculeCaracteristique(id: ID!, caracteristique: String!): Vehicule
  ajouterVehicule(modele: String!): Vehicule
  augmenterVehiculeQuantite(id: ID!): Vehicule
  reduireVehiculeQuantite(id: ID!): Vehicule
  supprimerVehicule(id: ID!): Boolean
  ajouterZone(zone: ZoneSaisie!): Zone
  modifierZone(id: ID!, zone: ZoneSaisie!): Zone
  supprimerZone(id: ID!): Boolean
  ajouterCollecte(
    collecte: CollecteSaisie!
    zoneId: ID!
    date: Date!
  ): Collecte!
  modifierCollecte(
    id: ID!
    collecte: CollecteSaisie!
    zoneId: ID!
    date: Date!
  ): Collecte!
  supprimerCollecte(id: ID!): ID
  modifierTournee(
    id: ID!
    configuration: TourneeConfigurationSaisie!
    date: Date
  ): Tournee!
  ajouterTourneeRecurrente(
    configuration: TourneeConfigurationSaisie!
    jour: Int!
  ): TourneeRecurrente!
  modifierTourneeRecurrente(
    id: ID!
    configuration: TourneeConfigurationSaisie!
    jour: Int!
  ): TourneeRecurrente!
  supprimerTourneeRecurrente(id: ID!): ID
}

type Vehicule {
  id: ID!
  modele: String!
  quantite: Int!
  caracteristiques: [String!]!
}

type Zone {
  id: ID!
  nom: String!
  communes: [String!]!
  tourneesRecurrentes: [TourneeRecurrente!]!
}

input ZoneSaisie {
  nom: String!
  communes: [String!]!
}

type Adresse {
  postale: String!
  longitude: Float!
  latitude: Float!
}

type Collecte {
  id: ID!
  nom: String!
  telephone: String!
  adresse: String!
  longitude: Float!
  latitude: Float!
  notes: String!
  encombrants: [String!]!
  tournee: Tournee!
  creation: Date!
  caracteristiques: [String!]!
}

input CollecteSaisie {
  nom: String!
  telephone: String!
  adresse: String!
  longitude: Float!
  latitude: Float!
  notes: String!
  encombrants: [String!]!
  caracteristiques: [String!]!
}

enum Creneau {
  MATIN
  APRESMIDI
  JOURNEE
}

type Tournee {
  id: ID
  date: Date!
  creneau: Creneau!
  zone: Zone!
  places: Int!
  placesReservees: Int!
  vehicules: [Vehicule!]!
}

type TourneeRecurrente {
  id: ID!
  creneau: Creneau!
  places: Int!
  jour: Int!
  vehicules: [Vehicule!]!
}

type Itineraire {
  geometrie: String!
  vehicule: Vehicule!
  collectes: [Collecte!]!
  duree: Int!
  distance: Int!
}

type LigneRapport {
  commune: String!
  nombres: [Int!]!
}

input TourneeVehiculeSaisie {
  id: ID!
  quantite: Int!
}

input TourneeConfigurationSaisie {
  zoneId: ID!
  creneau: Creneau!
  places: Int!
  vehicules: [TourneeVehiculeSaisie!]!
}
