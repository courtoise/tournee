package api

import (
	"encoding/json"
	"fmt"
	"time"
)

// Date is a custom GraphQL type
type Date struct {
	time.Time
}

// ImplementsGraphQLType maps this custom Go type
// to the graphql scalar type in the schema.
func (Date) ImplementsGraphQLType(name string) bool {
	return name == "Date"
}

// UnmarshalGraphQL is a custom unmarshaler for Time
//
// This function will be called whenever you use the
// time scalar as an input
func (d *Date) UnmarshalGraphQL(input interface{}) error {
	switch input := input.(type) {
	case time.Time:
		d.Time = input
		return nil
	case string:
		var err error
		d.Time, err = time.Parse("2006-01-02", input)
		return err
	default:
		return fmt.Errorf("wrong type")
	}
}

// MarshalJSON is a custom marshaler for Time
//
// This function will be called whenever you
// query for fields that use the Time type
func (d Date) MarshalJSON() ([]byte, error) {
	return json.Marshal(d.Format("2006-01-02"))
}
