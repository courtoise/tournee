package api

import (
	"io/ioutil"
	"log"

	"github.com/graph-gophers/graphql-go"
)

// NewSchema parse le schéma er retourne le schéma graphql
func NewSchema() *graphql.Schema {
	schemaFile, err := ioutil.ReadFile("schema.graphql")
	if err != nil {
		log.Fatalf("Erreur de lecture du schema")
	}

	opts := []graphql.SchemaOpt{graphql.UseFieldResolvers(), graphql.MaxParallelism(20)}
	schema := graphql.MustParseSchema(string(schemaFile), &Resolver{}, opts...)

	return schema
}
