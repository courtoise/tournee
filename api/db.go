package api

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
)

// DB est la connection à la base de données
var DB *gorm.DB

// InitDB initalise la connection à la base de données et migre le schéma
func InitDB() {
	postgresString := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
		os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_USER"), os.Getenv("DB_NAME"), os.Getenv("DB_PASSWORD"))

	// tryOpenDB := func() (*gorm.DB, error) {
	// 	attempts := 0
	// 	for {
	// 		attempts++
	// 		db, err := gorm.Open("postgres", postgresString)
	// 		if err != nil {
	// 			if attempts >= 5 {
	// 				return nil, err
	// 			}
	// 			time.Sleep(10 * time.Second)
	// 		} else {
	// 			return db, nil
	// 		}
	// 	}
	// }

	// db, err := tryOpenDB()
	// if err != nil {
	// 	panic(err)
	// }

	db, err := gorm.Open("postgres", postgresString)
	if err != nil {
		panic(err)
	}

	db.LogMode(os.Getenv("LOG") == "true")

	db.AutoMigrate(&Vehicule{}, &Zone{}, &ZoneCommune{}, &TourneeConfiguration{}, &Tournee{}, &Collecte{}, &TourneeVehicule{}, &TourneeRecurrente{}, &Caracteristique{})

	db.Model(&TourneeConfiguration{}).AddForeignKey("zone_id", "zones(id)", "RESTRICT", "RESTRICT")
	db.Model(&Tournee{}).AddForeignKey("configuration_id", "tournee_configurations(id)", "RESTRICT", "RESTRICT")
	db.Model(&TourneeRecurrente{}).AddForeignKey("configuration_id", "tournee_configurations(id)", "RESTRICT", "RESTRICT")
	db.Model(&ZoneCommune{}).AddForeignKey("zone_id", "zones(id)", "CASCADE", "CASCADE")
	db.Model(&TourneeVehicule{}).AddForeignKey("tournee_configuration_id", "tournee_configurations(id)", "CASCADE", "CASCADE")
	db.Model(&TourneeVehicule{}).AddForeignKey("vehicule_id", "vehicules(id)", "RESTRICT", "RESTRICT")
	db.Model(&Collecte{}).AddForeignKey("tournee_id", "tournees(id)", "RESTRICT", "RESTRICT")
	db.Table("vehicule_caracteristiques").AddForeignKey("vehicule_id", "vehicules(id)", "RESTRICT", "RESTRICT")
	db.Table("vehicule_caracteristiques").AddForeignKey("caracteristique_label", "caracteristiques(label)", "RESTRICT", "RESTRICT")
	db.Table("collecte_caracteristiques").AddForeignKey("collecte_id", "collectes(id)", "RESTRICT", "RESTRICT")
	db.Table("collecte_caracteristiques").AddForeignKey("caracteristique_label", "caracteristiques(label)", "RESTRICT", "RESTRICT")

	DB = db
}

// CloseDB ferme la connection à la base de données
func CloseDB() {
	DB.Close()
}
