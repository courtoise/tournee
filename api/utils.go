package api

import (
	"time"

	"github.com/gofrs/uuid"
	"github.com/graph-gophers/graphql-go"

	"github.com/rickar/cal/v2"
	"github.com/rickar/cal/v2/fr"
)

// VerifieID retourne une erreur si id n'est pas au bon format
func VerifieID(id graphql.ID) error {
	_, err := uuid.FromString(string(id))
	return err
}

// DateEqual permet de tester si les dates sont equivalentes
func DateEqual(date1, date2 time.Time) bool {
	y1, m1, d1 := date1.Date()
	y2, m2, d2 := date2.Date()
	return y1 == y2 && m1 == m2 && d1 == d2
}

// MonthRange retourne le premier et dernier jours du mois
func MonthRange(year, month int) (firstDay, lastDay time.Time) {
	firstDay = time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.Now().Location())
	lastDay = firstDay.AddDate(0, 1, -1)
	return
}

// EstTravaille permet de savoir si un jour est travaille
func EstTravaille(date *time.Time) bool {
	c := cal.NewBusinessCalendar()
	c.AddHoliday(fr.Holidays...)

	return c.IsWorkday(*date)
}
