package api

import (
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/golang/groupcache/lru"
)

// Resolver export
type Resolver struct{}

// GouvCache permet de limiter le nombre de requete vers data.gouv
type GouvCache struct {
	Adresse *lru.Cache
	Commune *lru.Cache
}

// Healthy permet de vérifier l'état du service
func (r *Resolver) Healthy(ctx context.Context) (bool, error) {
	err := DB.DB().Ping()
	if err != nil {
		return false, errors.New("Problème de connexion à la base de données")
	}
	return true, nil
}

// Adresse retourne une liste d'adresses correspondantes à la recherche
func (r *Resolver) Adresse(ctx context.Context, args struct{ Recherche string }) ([]Adresse, error) {
	gouvCache := ctx.Value("gouvCache").(*GouvCache)

	if adressesCached, ok := gouvCache.Adresse.Get(args.Recherche); ok {
		return adressesCached.([]Adresse), nil
	}

	resp, err := http.Get("https://api-adresse.data.gouv.fr/search?autocomplete=1&q=" + url.QueryEscape(args.Recherche))
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var gouvAdresses AdresseSearchResponse

	if err := json.Unmarshal(body, &gouvAdresses); err != nil {
		return nil, err
	}

	adresses := make([]Adresse, len(gouvAdresses.Features))

	for i, v := range gouvAdresses.Features {
		adresses[i] = Adresse{
			Postale:   v.Properties.Label,
			Longitude: v.Geometry.Coordinates[0],
			Latitude:  v.Geometry.Coordinates[1],
		}
	}

	gouvCache.Adresse.Add(args.Recherche, adresses)

	return adresses, nil
}

// Commune permet de rechercher une commune
func (r *Resolver) Commune(ctx context.Context, args struct{ Recherche string }) ([]string, error) {
	gouvCache := ctx.Value("gouvCache").(*GouvCache)

	if communesCached, ok := gouvCache.Commune.Get(args.Recherche); ok {
		return communesCached.([]string), nil
	}

	resp, err := http.Get("https://api-adresse.data.gouv.fr/search?type=municipality&autocomplete=0&q=" + url.QueryEscape(args.Recherche))
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var gouvAdresses AdresseSearchResponse

	if err := json.Unmarshal(body, &gouvAdresses); err != nil {
		return nil, err
	}

	communes := make([]string, len(gouvAdresses.Features))

	for i, v := range gouvAdresses.Features {
		communes[i] = v.Properties.Name
	}

	gouvCache.Commune.Add(args.Recherche, communes)

	return communes, nil
}
