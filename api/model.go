package api

import (
	"time"

	"github.com/gofrs/uuid"
	"github.com/graph-gophers/graphql-go"
	"github.com/lib/pq"
)

//Base est la base de tous les modeles
type Base struct {
	ID graphql.ID
}

//BeforeCreate permet de générer l'ID
func (b *Base) BeforeCreate() error {
	id, err := uuid.NewV4()
	if err != nil {
		return err
	}
	(*b).ID = graphql.ID(id.String())
	return nil
}

// Caracteristique modele
type Caracteristique struct {
	Label string `gorm:"PRIMARY_KEY;UNIQUE;NOT NULL"`
}

// Vehicule modele
type Vehicule struct {
	Base
	Modele           string            `gorm:"NOT NULL"`
	Quantite         int32             `gorm:"NOT NULL"`
	Caracteristiques []Caracteristique `gorm:"MANY2MANY:vehicule_caracteristiques;ASSOCIATION_FOREIGNKEY:label"`
}

// Zone modele
type Zone struct {
	Base
	Nom          string `gorm:"NOT NULL"`
	CommuneListe []ZoneCommune
}

// ZoneCommune modele
type ZoneCommune struct {
	ZoneID  graphql.ID `gorm:"PRIMARY_KEY"`
	Commune string     `gorm:"PRIMARY_KEY"`
}

//ZoneSaisie input
type ZoneSaisie struct {
	Nom      string
	Communes []string
}

// TourneeConfiguration modele
type TourneeConfiguration struct {
	Base
	ZoneID  graphql.ID `gorm:"NOT NULL"`
	Creneau string     `gorm:"NOT NULL"`
	Places  int32      `gorm:"NOT NULL"`
}

//Tournee modele
type Tournee struct {
	Base
	ConfigurationID graphql.ID `gorm:"NOT NULL"`
	Date            time.Time  `gorm:"NOT NULL;type:date"`
}

// TourneeRecurrente modele
type TourneeRecurrente struct {
	Base
	ConfigurationID graphql.ID `gorm:"NOT NULL"`
	Jour            int32      `gorm:"NOT NULL"`
}

//Collecte modele
type Collecte struct {
	Base
	TourneeID        graphql.ID        `gorm:"NOT NULL"`
	Nom              string            `gorm:"NOT NULL"`
	Telephone        string            `gorm:"NOT NULL"`
	Adresse          string            `gorm:"NOT NULL"`
	Longitude        float64           `gorm:"NOT NULL"`
	Latitude         float64           `gorm:"NOT NULL"`
	Encombrants      pq.StringArray    `gorm:"NOT NULL;type:text[]"`
	Notes            string            `gorm:"NOT NULL"`
	DateCreation     time.Time         `gorm:"NOT NULL;type:date"`
	Caracteristiques []Caracteristique `gorm:"MANY2MANY:collecte_caracteristiques;ASSOCIATION_FOREIGNKEY:label"`
}

//BeforeCreate permet de générer DateCreation
func (c *Collecte) BeforeCreate() error {
	if err := c.Base.BeforeCreate(); err != nil {
		return err
	}
	c.DateCreation = time.Now()
	return nil
}

//TourneeVehicule modele
type TourneeVehicule struct {
	TourneeConfigurationID graphql.ID `gorm:"PRIMARY_KEY"`
	VehiculeID             graphql.ID `gorm:"PRIMARY_KEY"`
	Quantite               int32      `gorm:"NOT NULL"`
}

//Adresse modele
type Adresse struct {
	Postale   string
	Longitude float64
	Latitude  float64
}

//AdresseSearchResponse modele
type AdresseSearchResponse struct {
	Type     string
	Version  string
	Features []AdresseSearchFeature
}

//AdresseSearchFeature modele
type AdresseSearchFeature struct {
	Geometry struct {
		Coordinates []float64
	}
	Properties struct {
		Label string
		Name  string
	}
}
