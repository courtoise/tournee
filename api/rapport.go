package api

import (
	"context"
)

// ILigneRapport modele
type ILigneRapport struct {
	Commune string
	Nombres []int32
}

// Rapport retourne le rapport d'un mois donné
func (r *Resolver) Rapport(ctx context.Context, args struct {
	Annee int32
}) ([]ILigneRapport, error) {

	var lignesBrut []struct {
		Commune string
		Nombre  int32
		Mois    int32
	}
	if err := DB.
		Raw(`select *, count(*) as nombre
		from (
			select (regexp_match(collectes.adresse, '\s\d{5}\s(.+)$'))[1] as commune, date_part('month', tournees.date) as mois from tournees 
			inner join collectes on tournees.id = collectes.tournee_id
			where date_part('year', tournees.date) = ?
		) as q 
		where commune notnull
		group by commune, mois
		order by mois, commune;
		`, args.Annee).
		Scan(&lignesBrut).
		Error; err != nil {
		return nil, err
	}

	communes := make(map[string][12]int32)
	for _, ligneBrut := range lignesBrut {
		t := communes[ligneBrut.Commune]
		t[ligneBrut.Mois-1] = ligneBrut.Nombre
		communes[ligneBrut.Commune] = t
	}

	var lignes []ILigneRapport
	for commune, nombres := range communes {
		nb := nombres
		lignes = append(lignes, ILigneRapport{
			Commune: commune,
			Nombres: nb[:],
		})
	}

	return lignes, nil
}
