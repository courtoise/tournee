module gitlab.com/courtoise/tournee/api

go 1.20

require (
	github.com/gofrs/uuid v4.4.0+incompatible
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da
	github.com/graph-gophers/graphql-go v1.5.0
	github.com/jinzhu/gorm v1.9.16
	github.com/lib/pq v1.10.9
	github.com/rickar/cal/v2 v2.1.13
	github.com/rs/cors v1.7.0
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/opentracing/opentracing-go v1.2.0 // indirect
	github.com/stretchr/testify v1.7.1 // indirect
)
