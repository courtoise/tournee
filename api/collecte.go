package api

import (
	"context"
	"errors"
	"time"

	"github.com/graph-gophers/graphql-go"
	"github.com/jinzhu/gorm"
)

//CollecteResolver resolver
type CollecteResolver struct {
	Collecte
}

//Creation CreationDate
func (c CollecteResolver) Creation() (Date, error) {
	return Date{
		Time: c.DateCreation,
	}, nil
}

//Caracteristiques CollecteResolver
func (c CollecteResolver) Caracteristiques() ([]string, error) {
	var caracteristiques []Caracteristique
	if err := DB.Model(&c.Collecte).Association("Caracteristiques").Find(&caracteristiques).Error; err != nil {
		return nil, err
	}
	var labels []string
	for _, caracteristique := range caracteristiques {
		labels = append(labels, caracteristique.Label)
	}
	return labels, nil
}

//CollecteSaise modele
type CollecteSaise struct {
	Nom              string
	Telephone        string
	Adresse          string
	Longitude        float64
	Latitude         float64
	Notes            string
	Encombrants      []string
	Caracteristiques []string
}

func toCollecteResolvers(collectes []Collecte) []CollecteResolver {
	var collecteResolvers []CollecteResolver
	for _, collecte := range collectes {
		collecteResolvers = append(collecteResolvers, CollecteResolver{
			collecte,
		})
	}
	return collecteResolvers
}

func trouveOuGenererTournee(tx *gorm.DB, zoneID *graphql.ID, date *Date) (*graphql.ID, error) {
	var tourneeID, configurationID struct {
		ID graphql.ID
	}
	if err := tx.Table("tournees").
		Select("tournees.id").
		Joins("left join tournee_configurations on tournees.configuration_id  = tournee_configurations.id").
		Where("tournees.date = ? AND tournee_configurations.zone_id = ?", date.Time, zoneID).
		Scan(&tourneeID).Error; err != nil {
		if !gorm.IsRecordNotFoundError(err) {
			return nil, err
		}

		if err := tx.Table("tournee_recurrentes").
			Select("tournee_configurations.id").
			Joins("left join tournee_configurations on tournee_recurrentes.configuration_id  = tournee_configurations.id").
			Where("tournee_recurrentes.jour = ? AND tournee_configurations.zone_id = ?", int(date.Weekday()), zoneID).
			Scan(&configurationID).Error; err != nil {
			if gorm.IsRecordNotFoundError(err) {
				return nil, errors.New("Tournée inexistante")
			}
			return nil, err
		}
	}

	if tourneeID.ID == "" && configurationID.ID != "" {
		tournee := Tournee{
			ConfigurationID: configurationID.ID,
			Date:            date.Time,
		}
		if err := tx.Create(&tournee).Error; err != nil {
			return nil, err
		}
		tourneeID.ID = tournee.ID
	}

	return &tourneeID.ID, nil
}

// AjouterCollecte ajoute une collecte à une tournée
func (r *Resolver) AjouterCollecte(ctx context.Context, args struct {
	ZoneID   graphql.ID
	Date     Date
	Collecte CollecteSaise
}) (*CollecteResolver, error) {
	var collecte Collecte
	var caracteristiques []Caracteristique
	if len(args.Collecte.Caracteristiques) > 0 {
		if err := DB.Where("label IN (?)", args.Collecte.Caracteristiques).Find(&caracteristiques).Error; err != nil {
			return nil, err
		}
	}
	err := DB.Transaction(func(tx *gorm.DB) error {
		tourneeID, err := trouveOuGenererTournee(tx, &args.ZoneID, &args.Date)
		if err != nil {
			return err
		}

		collecte = Collecte{
			TourneeID:        *tourneeID,
			Nom:              args.Collecte.Nom,
			Telephone:        args.Collecte.Telephone,
			Adresse:          args.Collecte.Adresse,
			Longitude:        args.Collecte.Longitude,
			Latitude:         args.Collecte.Latitude,
			Notes:            args.Collecte.Notes,
			Encombrants:      args.Collecte.Encombrants,
			Caracteristiques: caracteristiques,
		}
		if err := tx.Create(&collecte).Error; err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return nil, err
	}

	return &CollecteResolver{collecte}, nil
}

// ModifierCollecte modifier une collecte existante
func (r *Resolver) ModifierCollecte(ctx context.Context, args struct {
	ID       graphql.ID
	ZoneID   graphql.ID
	Date     Date
	Collecte CollecteSaise
}) (*CollecteResolver, error) {
	var collecte Collecte
	var caracteristiques []Caracteristique
	if len(args.Collecte.Caracteristiques) > 0 {
		if err := DB.Where("label IN (?)", args.Collecte.Caracteristiques).Find(&caracteristiques).Error; err != nil {
			return nil, err
		}
	}
	err := DB.Transaction(func(tx *gorm.DB) error {

		if err := DB.Where("id = ?", args.ID).Take(&collecte).Error; err != nil {
			return err
		}

		tourneeID, err := trouveOuGenererTournee(tx, &args.ZoneID, &args.Date)
		if err != nil {
			return err
		}

		collecte.TourneeID = *tourneeID
		collecte.Nom = args.Collecte.Nom
		collecte.Telephone = args.Collecte.Telephone
		collecte.Adresse = args.Collecte.Adresse
		collecte.Longitude = args.Collecte.Longitude
		collecte.Latitude = args.Collecte.Latitude
		collecte.Notes = args.Collecte.Notes
		collecte.Encombrants = args.Collecte.Encombrants

		if err := tx.Save(&collecte).Error; err != nil {
			return err
		}

		if err := tx.Model(&collecte).Association("Caracteristiques").Replace(caracteristiques).Error; err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return nil, err
	}

	return &CollecteResolver{collecte}, nil
}

// Collecte retourne une collecte
func (r *Resolver) Collecte(ctx context.Context, args struct{ ID graphql.ID }) (*CollecteResolver, error) {
	var collecte Collecte
	if err := DB.Where("id = ?", args.ID).Take(&collecte).Error; err != nil {
		return nil, err
	}
	return &CollecteResolver{collecte}, nil
}

// Collectes retournes les collectes associés à une tournées
func (r *Resolver) Collectes(ctx context.Context, args struct {
	TourneeID *graphql.ID
}) ([]CollecteResolver, error) {
	var collectes []Collecte

	if args.TourneeID != nil {
		if err := DB.Where("tournee_id = ?", *args.TourneeID).Find(&collectes).Error; err != nil {
			return nil, err
		}
	} else {
		if err := DB.
			Table("tournees").
			Select("collectes.*").
			Joins("INNER JOIN collectes ON tournees.id = collectes.tournee_id").
			Where("tournees.date >= ?", time.Now()).
			Scan(&collectes).Error; err != nil {
			return nil, err
		}
	}

	return toCollecteResolvers(collectes), nil
}

// SupprimerCollecte permet de supprimer une collecte
func (r *Resolver) SupprimerCollecte(ctx context.Context, args struct{ ID graphql.ID }) (*graphql.ID, error) {
	if err := DB.Delete(&Collecte{}, "id = ?", args.ID).Error; err != nil {
		return nil, err
	}

	return &args.ID, nil
}

// Tournee retourne la tournee assocciée à la collecte
func (c Collecte) Tournee(ctx context.Context) (*ITournee, error) {
	var tournee Tournee
	if err := DB.Where("id = ?", c.TourneeID).Find(&tournee).Error; err != nil {
		return nil, err
	}

	return tournee.toITournee(), nil
}
