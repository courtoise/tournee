package api

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/graph-gophers/graphql-go"
)

// VroomInput api model
type VroomInput struct {
	Jobs     []VroomJob     `json:"jobs"`
	Vehicles []VroomVehicle `json:"vehicles"`
	Options  struct {
		G bool `json:"g"`
	} `json:"options"`
}

// VroomJob api model
type VroomJob struct {
	ID       int        `json:"id"`
	Location [2]float64 `json:"location"`
	Pickup   []int      `json:"pickup"`
	Skills   []int      `json:"skills"`
}

// VroomVehicle api model
type VroomVehicle struct {
	ID       int        `json:"id"`
	Start    [2]float64 `json:"start"`
	End      [2]float64 `json:"end"`
	Capacity []int      `json:"capacity"`
	Skills   []int      `json:"skills"`
}

// VroomOutput api model
type VroomOutput struct {
	Code       int          `json:"code"`
	Error      string       `json:"error"`
	Unassigned []VroomJob   `json:"unassigned"`
	Routes     []VroomRoute `json:"routes"`
}

// VroomRoute api model
type VroomRoute struct {
	Vehicle  int         `json:"vehicle"`
	Steps    []VroomStep `json:"steps"`
	Geometry string      `json:"geometry"`
	Distance int         `json:"distance"`
	Duration int         `json:"duration"`
}

// VroomStep api model
type VroomStep struct {
	Type string `json:"type"`
	Job  int    `json:"job"`
}

// Itineraire gql modele
type Itineraire struct {
	Geometrie string
	Vehicule  VehiculeResolver
	Collectes []CollecteResolver
	Duree     int32
	Distance  int32
}

// Itineraires retourne les itineraires d'une tournée
func (r *Resolver) Itineraires(ctx context.Context, args struct{ TourneeID graphql.ID }) ([]Itineraire, error) {

	var tournee Tournee
	if err := DB.Take(&tournee, "id = ?", args.TourneeID).Error; err != nil {
		return nil, err
	}

	var vehicules []Vehicule
	if err := DB.Preload("Caracteristiques").Table("tournee_vehicules").
		Select("vehicules.id, vehicules.modele, tournee_vehicules.quantite").
		Joins("left join vehicules on tournee_vehicules.vehicule_id  = vehicules.id").
		Where("tournee_vehicules.tournee_configuration_id = ?", tournee.ConfigurationID).
		Find(&vehicules).Error; err != nil {
		return nil, err
	}

	var collectes []Collecte
	if err := DB.Preload("Caracteristiques").Find(&collectes, "tournee_id = ?", args.TourneeID).Error; err != nil {
		return nil, err
	}

	var caracteristiques []Caracteristique
	if err := DB.Find(&caracteristiques).Error; err != nil {
		return nil, err
	}

	caracteristiqueMap := make(map[string]int)
	for i, caracteristique := range caracteristiques {
		caracteristiqueMap[caracteristique.Label] = i + 1
	}

	input := VroomInput{
		Jobs: make([]VroomJob, len(collectes)),
	}
	input.Options.G = true

	for i, collecte := range collectes {
		input.Jobs[i] = VroomJob{
			ID:     i,
			Pickup: []int{1},
			Location: [2]float64{
				collecte.Longitude,
				collecte.Latitude,
			},
			Skills: []int{0},
		}

		for _, caracteristique := range collecte.Caracteristiques {
			input.Jobs[i].Skills = append(input.Jobs[i].Skills, caracteristiqueMap[caracteristique.Label])
		}
	}

	var vehiculesQuantite int
	for _, v := range vehicules {
		vehiculesQuantite += int(v.Quantite)
	}
	capacity := len(collectes)/vehiculesQuantite + 1
	courtoiseCoord := [2]float64{
		5.8878328, 43.44987,
	}
	vehiculeMap := make(map[int]Vehicule)
	vID := 0
	for _, v := range vehicules {
		for i := 0; i < int(v.Quantite); i++ {
			vroomV := VroomVehicle{
				ID:       vID,
				Capacity: []int{capacity},
				Start:    courtoiseCoord,
				End:      courtoiseCoord,
				Skills:   []int{0},
			}
			for _, caracteristique := range v.Caracteristiques {
				vroomV.Skills = append(vroomV.Skills, caracteristiqueMap[caracteristique.Label])
			}
			input.Vehicles = append(input.Vehicles, vroomV)

			vehiculeMap[vID] = v
			vID++
		}
	}

	bodyReq, err := json.Marshal(input)
	if err != nil {
		return nil, err
	}

	resp, err := http.Post("http://"+os.Getenv("VROOM_HOST"), "application/json", bytes.NewReader(bodyReq))
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	bodyRes, err := ioutil.ReadAll(resp.Body)

	var output VroomOutput
	if err := json.Unmarshal(bodyRes, &output); err != nil {
		return nil, err
	}

	if output.Code != 0 {
		return nil, errors.New(output.Error)
	}

	if len(output.Unassigned) > 0 {
		return nil, errors.New("Des collectes n'ont pas pu être assignés")
	}

	itineraires := make([]Itineraire, len(output.Routes))
	for i, route := range output.Routes {
		itineraires[i] = Itineraire{
			Geometrie: route.Geometry,
			Vehicule:  VehiculeResolver{vehiculeMap[route.Vehicle]},
			Collectes: make([]CollecteResolver, len(route.Steps)-2),
			Duree:     int32(route.Duration),
			Distance:  int32(route.Distance),
		}
		for j, step := range route.Steps {
			if step.Type == "job" {
				itineraires[i].Collectes[j-1] = CollecteResolver{collectes[step.Job]}
			}
		}
	}

	return itineraires, nil
}
