package api

import (
	"context"

	"github.com/graph-gophers/graphql-go"
)

// AjouterCaracteristique ajoute une caractérique à un véhicule
func (r *Resolver) AjouterCaracteristique(ctx context.Context, args struct{ Label string }) (*string, error) {
	caracteristique := Caracteristique{Label: args.Label}

	if err := DB.Create(&caracteristique).Error; err != nil {
		return nil, err
	}
	return &caracteristique.Label, nil
}

// SupprimerCaracteristique Supprime une Caracteristique
func (r *Resolver) SupprimerCaracteristique(ctx context.Context, args struct{ Label string }) (*bool, error) {

	success := true

	if err := DB.Where("label = ?", args.Label).Delete(Caracteristique{}).Error; err != nil {
		success = false
		return &success, err
	}
	return &success, nil
}

// ModifierCaracteristique modifie le caracteristique ayant l'ID passer en argument
func (r *Resolver) ModifierCaracteristique(ctx context.Context, args struct {
	AncienLabel  string
	NouveauLabel string
}) (*string, error) {
	var caracteristique Caracteristique

	if err := DB.Where("label = ?", args.AncienLabel).Take(&caracteristique).Error; err != nil {
		return nil, err
	}
	caracteristique.Label = args.NouveauLabel
	if err := DB.Save(&caracteristique).Error; err != nil {
		return nil, err
	}

	return &caracteristique.Label, nil
}

// Caracteristiques retourne l'ensemble des Caractéristiques
func (r *Resolver) Caracteristiques(ctx context.Context) ([]string, error) {
	var labels []string

	if err := DB.Model(&Caracteristique{}).Pluck("label", &labels).Error; err != nil {
		return nil, err
	}
	return labels, nil
}

//AjouterVehiculeCaracteristique
func (r *Resolver) AjouterVehiculeCaracteristique(ctx context.Context, args struct {
	ID              graphql.ID
	Caracteristique string
}) (*VehiculeResolver, error) {

	var vehicule Vehicule
	var caracteristique Caracteristique

	if err := DB.Where("label = ?", args.Caracteristique).Take(&caracteristique).Error; err != nil {
		return nil, err
	}

	if err := DB.Where("id = ?", args.ID).Take(&vehicule).Error; err != nil {
		return nil, err
	}

	if err := DB.Model(&vehicule).Association("Caracteristiques").Append(&caracteristique).Error; err != nil {
		return nil, err
	}
	return &VehiculeResolver{vehicule}, nil
}

//EnleverVehiculeCaracteristique
func (r *Resolver) EnleverVehiculeCaracteristique(ctx context.Context, args struct {
	ID              graphql.ID
	Caracteristique string
}) (*VehiculeResolver, error) {

	var vehicule Vehicule
	var caracteristique Caracteristique

	if err := DB.Where("label = ?", args.Caracteristique).Take(&caracteristique).Error; err != nil {
		return nil, err
	}

	if err := DB.Where("id = ?", args.ID).Take(&vehicule).Error; err != nil {
		return nil, err
	}

	if err := DB.Model(&vehicule).Association("Caracteristiques").Delete(&caracteristique).Error; err != nil {
		return nil, err
	}
	return &VehiculeResolver{vehicule}, nil
}
