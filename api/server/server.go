package main

import (
	"context"
	"log"
	"net/http"

	"github.com/golang/groupcache/lru"
	"github.com/graph-gophers/graphql-go/relay"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/rs/cors"
	"gitlab.com/courtoise/tournee/api"
)

func main() {
	api.InitDB()
	defer api.CloseDB()

	schema := api.NewSchema()

	gouvCache := &api.GouvCache{
		Adresse: lru.New(1000),
		Commune: lru.New(500),
	}

	baseHandler := cors.Default().Handler(&relay.Handler{Schema: schema})
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		ctx = context.WithValue(ctx, "gouvCache", gouvCache)

		baseHandler.ServeHTTP(w, r.WithContext(ctx))
	})

	http.Handle("/graphql", handler)

	log.Fatal(http.ListenAndServe(":9011", nil))
}
