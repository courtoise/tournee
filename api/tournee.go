package api

import (
	"context"
	"errors"
	"time"

	"github.com/graph-gophers/graphql-go"
	"github.com/jinzhu/gorm"
)

// ITourneeConfigurationBase
type ITourneeConfigurationBase struct {
	ZoneID          graphql.ID
	ConfigurationID graphql.ID
	Creneau         string
	Places          int32
}

// ITournee modele gql
type ITournee struct {
	ITourneeConfigurationBase
	ID        *graphql.ID
	DateBrute time.Time
}

// ITourneeRecurrente modele gql
type ITourneeRecurrente struct {
	ITourneeConfigurationBase
	ID   graphql.ID
	Jour int32
}

// ITourneeConfigurationSaise modele gql
type ITourneeConfigurationSaise struct {
	ITourneeConfigurationBase
	Vehicules []struct {
		ID       graphql.ID
		Quantite int32
	}
}

func (t Tournee) toITournee() *ITournee {
	it := ITournee{
		ID:        &t.ID,
		DateBrute: t.Date,
	}
	it.ConfigurationID = t.ConfigurationID
	return &it
}

// Zone retourne la zone de la tournée
func (tournee ITournee) Zone(ctx context.Context) (*Zone, error) {
	var zone Zone

	zoneID := tournee.ZoneID
	if zoneID == "" {
		var configuration TourneeConfiguration
		if err := DB.Take(&configuration, "id = ?", tournee.ConfigurationID).Error; err != nil {
			return nil, err
		}
		zoneID = configuration.ZoneID
	}

	if err := DB.Take(&zone, "id = ?", zoneID).Error; err != nil {
		return nil, err
	}

	return &zone, nil
}

// Date retourne l'année de la tournée
func (tournee ITournee) Date(ctx context.Context) (Date, error) {
	return Date{Time: tournee.DateBrute}, nil
}

// PlacesReservees retourne le nombre places reservees
func (tournee ITournee) PlacesReservees(ctx context.Context) (int32, error) {
	if tournee.ID != nil {
		var nombre int32
		if err := DB.Model(&Collecte{}).Where("tournee_id = ?", tournee.ID).Count(&nombre).Error; err != nil {
			return 0, err
		}
		return nombre, nil
	}

	return 0, nil
}

// Vehicules retourne
func (tournee ITournee) Vehicules(ctx context.Context) ([]VehiculeResolver, error) {
	var vehicules []Vehicule

	if tournee.ID != nil {
		db := DB.Table("tournee_vehicules").
			Select("tournee_vehicules.quantite, vehicules.id, vehicules.modele").
			Joins("INNER JOIN vehicules ON tournee_vehicules.vehicule_id = vehicules.id").
			Where("tournee_vehicules.tournee_configuration_id = ?", tournee.ConfigurationID)

		if err := db.Scan(&vehicules).Error; err != nil {
			return nil, err
		}
	}

	return toVehiculeResolvers(vehicules), nil
}

func tourneesReq() *gorm.DB {
	return DB.Table("tournees").
		Select("tournees.id, tournees.configuration_id, tournees.date as date_brute, tournee_configurations.zone_id, tournee_configurations.creneau, tournee_configurations.places").
		Joins("left join tournee_configurations on tournees.configuration_id  = tournee_configurations.id")
}

// Tournee retourne la tournee
func (r *Resolver) Tournee(ctx context.Context, args struct{ ID graphql.ID }) (*ITournee, error) {
	var tournee ITournee
	if err := tourneesReq().Where("tournees.id = ?", args.ID).Take(&tournee).Error; err != nil {
		return nil, err
	}

	return &tournee, nil
}

// Tournees retourne la liste des tournées
func (r *Resolver) Tournees(ctx context.Context, args struct {
	ZoneID    *graphql.ID
	Annee     int32
	Mois      int32
	Existante *bool
}) ([]ITournee, error) {
	premierJour, dernierJour := MonthRange(int(args.Annee), int(args.Mois))

	req1 := tourneesReq().
		Where("tournees.date >= ? AND tournees.date <= ?", premierJour, dernierJour)
	req2 := tourneeRecurrenteReq()

	if args.ZoneID != nil {
		req1 = req1.Where("zone_id = ?", *args.ZoneID)
		req2 = req2.Where("zone_id = ?", *args.ZoneID)
	}

	var tournees []ITournee
	if err := req1.Order("date_brute").Find(&tournees).Error; err != nil {
		return nil, err
	}

	if args.Existante != nil && *args.Existante == true {
		return tournees, nil
	}

	var tourneesRecurrentes []ITourneeRecurrente
	if err := req2.Find(&tourneesRecurrentes).Error; err != nil {
		return nil, err
	}

	if time.Now().After(premierJour) {
		premierJour = time.Now()
	}
	var toutesTournees []ITournee
	if len(tourneesRecurrentes) > 0 {
		date := premierJour
		for i := date.Day(); i <= dernierJour.Day(); i++ {
			for _, tourneeRecurrente := range tourneesRecurrentes {
				if tourneeRecurrente.Jour == int32(date.Weekday()) {
					tourneePresente := false
					for _, tournee := range tournees {
						if tournee.ZoneID == tourneeRecurrente.ZoneID && DateEqual(tournee.DateBrute, date) {
							tourneePresente = true
							break
						}
					}
					if tourneePresente != true && EstTravaille(&date) {
						t := ITournee{
							DateBrute: date,
						}
						t.ConfigurationID = tourneeRecurrente.ConfigurationID
						t.Creneau = tourneeRecurrente.Creneau
						t.Places = tourneeRecurrente.Places
						t.ZoneID = tourneeRecurrente.ZoneID

						toutesTournees = append(toutesTournees, t)
					}
				}

			}
			date = date.AddDate(0, 0, 1)
		}
	}

	toutesTournees = append(toutesTournees, tournees...)

	return toutesTournees, nil
}

// ModifierTournee permet d'ajuster la tournee
func (r *Resolver) ModifierTournee(ctx context.Context, args struct {
	ID            graphql.ID
	Configuration ITourneeConfigurationSaise
	Date          *Date
}) (*ITournee, error) {
	var tournee Tournee
	if err := DB.Where("id = ?", args.ID).Take(&tournee).Error; err != nil {
		return nil, err
	}

	ancienneConfigurationID := tournee.ConfigurationID
	var configuration TourneeConfiguration
	err := DB.Transaction(func(tx *gorm.DB) error {
		if err := args.Configuration.genere(tx, &configuration); err != nil {
			return err
		}

		tournee.ConfigurationID = configuration.ID
		if args.Date != nil {
			tournee.Date = args.Date.Time
		}
		if err := tx.Save(&tournee).Error; err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return nil, err
	}

	DB.Delete(&TourneeConfiguration{}, "id = ?", ancienneConfigurationID)

	itournee := ITournee{
		ID:        &tournee.ID,
		DateBrute: tournee.Date,
	}
	itournee.Creneau = configuration.Creneau
	itournee.Places = configuration.Places
	itournee.ZoneID = configuration.ZoneID
	itournee.ConfigurationID = configuration.ID

	return &itournee, nil

}

func tourneeRecurrenteReq() *gorm.DB {
	return DB.Table("tournee_recurrentes").
		Select("tournee_recurrentes.id, tournee_recurrentes.jour, tournee_recurrentes.configuration_id, tournee_configurations.zone_id, tournee_configurations.creneau, tournee_configurations.places").
		Joins("left join tournee_configurations on tournee_recurrentes.configuration_id  = tournee_configurations.id")
}

// TourneesRecurrentes retourne les tournées récurrentes assiociés à la zone
func (zone Zone) TourneesRecurrentes() ([]ITourneeRecurrente, error) {
	var tournees []ITourneeRecurrente
	if err := tourneeRecurrenteReq().Order("id").Where("zone_id = ?", zone.ID).Find(&tournees).Error; err != nil {
		return nil, err
	}

	return tournees, nil
}

// Vehicules retourne les vehicules de tournée
func (tournee ITourneeRecurrente) Vehicules(ctx context.Context) ([]VehiculeResolver, error) {
	var vehicules []Vehicule

	req := DB.Table("tournee_vehicules").
		Select("tournee_vehicules.quantite, vehicules.id, vehicules.modele").
		Joins("INNER JOIN vehicules ON tournee_vehicules.vehicule_id = vehicules.id").
		Where("tournee_vehicules.tournee_configuration_id = ?", tournee.ConfigurationID)

	if err := req.Scan(&vehicules).Error; err != nil {
		return nil, err
	}

	return toVehiculeResolvers(vehicules), nil
}

func (c *ITourneeConfigurationSaise) genere(tx *gorm.DB, configuration *TourneeConfiguration) error {
	*configuration = TourneeConfiguration{
		ZoneID:  c.ZoneID,
		Creneau: c.Creneau,
		Places:  c.Places,
	}

	if err := tx.Create(configuration).Error; err != nil {
		return err
	}

	for _, v := range c.Vehicules {
		if err := tx.Create(&TourneeVehicule{
			TourneeConfigurationID: configuration.ID,
			VehiculeID:             v.ID,
			Quantite:               v.Quantite,
		}).Error; err != nil {
			return err
		}
	}

	return nil
}

// AjouterTourneeRecurrente ajoute une tournee recurrente
func (r *Resolver) AjouterTourneeRecurrente(ctx context.Context, args struct {
	Configuration ITourneeConfigurationSaise
	Jour          int32
}) (*ITourneeRecurrente, error) {
	var nombreMemeJour int32
	if err := tourneeRecurrenteReq().Where("zone_id = ? AND jour = ?", args.Configuration.ZoneID, args.Jour).Count(&nombreMemeJour).Error; err != nil {
		return nil, err
	}
	if nombreMemeJour > 0 {
		return nil, errors.New("Il existe déjà une tournée récurrente ce jour là")
	}

	var tournee TourneeRecurrente
	var configuration TourneeConfiguration
	err := DB.Transaction(func(tx *gorm.DB) error {
		if err := args.Configuration.genere(tx, &configuration); err != nil {
			return err
		}

		tournee := TourneeRecurrente{
			ConfigurationID: configuration.ID,
			Jour:            args.Jour,
		}
		if err := tx.Create(&tournee).Error; err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return nil, err
	}

	resultat := ITourneeRecurrente{
		ID:   tournee.ID,
		Jour: tournee.Jour,
	}
	resultat.ConfigurationID = configuration.ID
	resultat.ZoneID = configuration.ZoneID
	resultat.Creneau = configuration.Creneau
	resultat.Places = configuration.Places

	return &resultat, nil
}

// ModifierTourneeRecurrente ajoute une tournee recurrente
func (r *Resolver) ModifierTourneeRecurrente(ctx context.Context, args struct {
	ID            graphql.ID
	Configuration ITourneeConfigurationSaise
	Jour          int32
}) (*ITourneeRecurrente, error) {
	var tournee TourneeRecurrente
	if err := DB.Where("id = ?", args.ID).Take(&tournee).Error; err != nil {
		return nil, err
	}

	if tournee.Jour != args.Jour {
		var nombreMemeJour int32
		if err := tourneeRecurrenteReq().Where("zone_id = ? AND jour = ?", args.Configuration.ZoneID, args.Jour).Count(&nombreMemeJour).Error; err != nil {
			return nil, err
		}
		if nombreMemeJour > 0 {
			return nil, errors.New("Il existe déjà une tournée récurrente ce jour là")
		}
	}

	ancienneConfigurationID := tournee.ConfigurationID
	var configuration TourneeConfiguration
	err := DB.Transaction(func(tx *gorm.DB) error {
		if err := args.Configuration.genere(tx, &configuration); err != nil {
			return err
		}

		tournee.ConfigurationID = configuration.ID
		tournee.Jour = args.Jour
		if err := tx.Save(&tournee).Error; err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return nil, err
	}

	DB.Delete(&TourneeConfiguration{}, "id = ?", ancienneConfigurationID)

	resultat := ITourneeRecurrente{
		ID:   tournee.ID,
		Jour: tournee.Jour,
	}
	resultat.ConfigurationID = configuration.ID
	resultat.ZoneID = configuration.ZoneID
	resultat.Creneau = configuration.Creneau
	resultat.Places = configuration.Places

	return &resultat, nil
}

// SupprimerTourneeRecurrente supprime une tournee recurrente
func (r *Resolver) SupprimerTourneeRecurrente(ctx context.Context, args struct {
	ID graphql.ID
}) (*graphql.ID, error) {
	var tournee TourneeRecurrente
	if err := DB.Where("id = ?", args.ID).Take(&tournee).Error; err != nil {
		return nil, err
	}

	ancienneConfigurationID := tournee.ConfigurationID
	if err := DB.Delete(&tournee).Error; err != nil {
		return nil, err
	}

	DB.Delete(&TourneeConfiguration{}, "id = ?", ancienneConfigurationID)

	return &tournee.ID, nil
}
