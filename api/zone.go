package api

import (
	"context"
	"errors"

	"github.com/graph-gophers/graphql-go"
)

//Zones list les zones
func (r *Resolver) Zones(ctx context.Context) ([]Zone, error) {
	var zones []Zone

	if err := DB.Find(&zones).Error; err != nil {
		return nil, err
	}

	return zones, nil
}

// Communes d'une zone
func (zone Zone) Communes(ctx context.Context) ([]string, error) {
	var communes []string

	if err := DB.Model(&ZoneCommune{}).Where("zone_id = ?", zone.ID).Pluck("commune", &communes).Error; err != nil {
		return nil, err
	}

	return communes, nil
}

//AjouterZone ajoute une zone
func (r *Resolver) AjouterZone(ctx context.Context, args struct{ Zone ZoneSaisie }) (*Zone, error) {
	zone := Zone{
		Nom:          args.Zone.Nom,
		CommuneListe: make([]ZoneCommune, len(args.Zone.Communes)),
	}

	for i, v := range args.Zone.Communes {
		zone.CommuneListe[i] = ZoneCommune{
			Commune: v,
		}
	}

	if err := DB.Create(&zone).Error; err != nil {
		return nil, err
	}

	return &zone, nil
}

//ModifierZone modifie une zone
func (r *Resolver) ModifierZone(ctx context.Context, args struct {
	ID   graphql.ID
	Zone ZoneSaisie
}) (*Zone, error) {

	var count int
	if err := DB.Model(&Zone{}).Where("id = ?", args.ID).Count(&count).Error; err != nil {
		return nil, err
	}

	if count == 0 {
		return nil, errors.New("Zone non trouvée")
	}

	zone := Zone{
		Nom:          args.Zone.Nom,
		CommuneListe: make([]ZoneCommune, len(args.Zone.Communes)),
	}

	zone.ID = args.ID

	for i, v := range args.Zone.Communes {
		zone.CommuneListe[i] = ZoneCommune{
			Commune: v,
		}
	}

	if err := DB.Where("zone_id = ?", args.ID).Not("commune", args.Zone.Communes).Delete(&ZoneCommune{}).Error; err != nil {
		return nil, err
	}

	if err := DB.Save(&zone).Error; err != nil {
		return nil, err
	}

	return &zone, nil
}

//SupprimerZone supprime une zone
func (r *Resolver) SupprimerZone(ctx context.Context, args struct{ ID graphql.ID }) (*bool, error) {
	if err := VerifieID(args.ID); err != nil {
		return nil, err
	}

	if err := DB.Delete(&Zone{}, "id = ?", args.ID).Error; err != nil {
		return nil, err
	}

	success := true
	return &success, nil
}