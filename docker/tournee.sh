#!/bin/sh

MODE=${2:-prod} 

install() {
    docker volume create osrm
    docker volume create openstreetmap

    updateMap
    update
}

start() {
    docker-compose -f docker-compose.yml -f docker-compose.$MODE.yml -p tournee up -d --remove-orphans
}

stop() {
    docker-compose -f docker-compose.yml -f docker-compose.$MODE.yml -p tournee stop
}

update() {
    backup
    docker-compose -f docker-compose.yml -f docker-compose.$MODE.yml -p tournee pull
    start
}

updateMap() {
    docker run --rm -v $(pwd)/geofabrik:/geofabrik usualstuff/wget wget -NP /geofabrik "http://download.geofabrik.de/europe/france/provence-alpes-cote-d-azur.poly" "http://download.geofabrik.de/europe/france/provence-alpes-cote-d-azur-latest.osm.pbf"

    stop
    docker run --rm -v osrm:/data -v $(pwd)/geofabrik:/geofabrik alpine sh -c "cp /geofabrik/* /data/"
    docker-compose -f docker-compose.yml -f docker-compose.$MODE.yml run osrm-extract -p /opt/car.lua /data/provence-alpes-cote-d-azur-latest.osm.pbf
    docker-compose -f docker-compose.yml -f docker-compose.$MODE.yml run osrm-partition /data/provence-alpes-cote-d-azur-latest.osm.pbf
    docker-compose -f docker-compose.yml -f docker-compose.$MODE.yml run osrm-customize /data/provence-alpes-cote-d-azur-latest.osm.pbf
    docker run --rm -v openstreetmap:/var/lib/postgresql/15/main -v $(pwd)/geofabrik/provence-alpes-cote-d-azur-latest.osm.pbf:/data/region.osm.pbf -v $(pwd)/geofabrik/provence-alpes-cote-d-azur.poly:/data/region.poly overv/openstreetmap-tile-server import
    start
}

backup() {
    docker-compose -f docker-compose.yml -f docker-compose.$MODE.yml -p tournee exec db-backup /backup.sh
}

case $1 in
    install) install;;
    start) start;;
    stop) stop;;
    update) update;;
    update-map) updateMap;;
    backup) backup;;
    *) echo "install start update update-map backup stop ?";;
esac